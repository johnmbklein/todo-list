import org.fluentlenium.adapter.FluentTest;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import static org.fluentlenium.core.filter.FilterConstructor.*;

import static org.assertj.core.api.Assertions.assertThat;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }

  @ClassRule
  public static ServerRule server = new ServerRule();

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("Tasks:");
  }

  @Test
  public void taskIsCreatedTest() {
    goTo("http://localhost:4567/");
    click("a", withText("Add New Task"));
    fill("#name").with("Mow the lawn");
    submit(".btn");
    assertThat(pageSource()).contains("Task added!");
  }

  @Test
  public void taskIsDisplayedTest() {
    goTo("http://localhost:4567/tasks/new");
    fill("#name").with("Mow the Lawn");
    submit(".btn");
    click("a", withText("View tasks"));
    assertThat(pageSource()).contains("Mow the Lawn");
  }

  @Test
  public void multipleTasksAreDisplayedTest() {
    goTo("http://localhost:4567/tasks/new");
    fill("#name").with("Mow the Lawn");
    submit(".btn");
    click("a", withText("View tasks"));
    goTo("http://localhost:4567/tasks/new");
    fill("#name").with("Get Milk");
    submit(".btn");
    click("a", withText("View tasks"));
    assertThat(pageSource()).contains("Mow the Lawn");
    assertThat(pageSource()).contains("Get Milk");
  }

  @Test
  public void taskShowPageDisplaysName() {
    goTo("http://localhost:4567/tasks/new");
    fill("#name").with("Wash car");
    submit(".btn");
    click("a", withText("View tasks"));
    click("a", withText("Wash car"));
    assertThat(pageSource()).contains("Wash car");
  }

  @Test
  public void taskNotFoundMessageShown() {
    Task.clear();
    goTo("http://localhost:4567/tasks/999");
    assertThat(pageSource()).contains("Task not found");
  }


}
