import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;
import java.util.ArrayList;
import java.time.LocalDateTime;


public class TaskTest {

  @Test
  public void Task_instantiatesCorrectly_true(){
    Task testTask = new Task("Mow the Lawn");
    assertEquals(true, testTask instanceof Task);
  }

  @Test
  public void Task_instantiatesWithDescription_true(){
    Task testTask = new Task("Mow the Lawn");
    assertEquals("Mow the Lawn", testTask.getName());
  }

  @Test
  public void Task_instantiatesWithCorrectID_true(){
    Task testTask = new Task("Mow the Lawn");
    assertEquals(Task.all().size(), testTask.getID());
  }

  @Test
  public void isCompleted_isFalseAfterInstantiation_false() {
    Task testTask = new Task("Mow the lawn");
    assertEquals(false, testTask.isCompleted());
  }

  @Test
  public void getCreatedAt_instantiantesWithCurrentTime_today() {
    Task testTask = new Task("Mow the lawn");
    assertEquals(LocalDateTime.now().getDayOfWeek(), testTask.getCreatedAt().getDayOfWeek());
  }

  @Test
  public void all_returnsAllInstancesOfTask_true() {
    Task firstTestTask = new Task("Mow the lawn");
    Task secondTestTask = new Task("Get Milk");
    assertEquals(true, Task.all().contains(firstTestTask));
    assertEquals(true, Task.all().contains(secondTestTask));
  }

  @Test
  public void clear_emptiesAllTasksFromTaskArray_0() {
    Task testTask = new Task("Mow the Lawn");
    Task.clear();
    assertEquals(0, Task.all().size());
  }

  @Test
  public void getID_returnsTaskID_1() {
    Task.clear();
    Task testTask = new Task("Mow the Lawn");
    assertEquals(1, testTask.getID());
  }

  @Test
  public void findByID_returnsTaskWithSameID_secondTask() {
    Task.clear();
    Task firstTestTask = new Task("Mow the lawn");
    Task secondTestTask = new Task("Get Milk");
    assertEquals(secondTestTask, Task.findByID(2));
  }

  @Test
  public void findByID_returnsNullIfIndexOutOfBoundsException_null() {
    Task.clear();
    Task testTask = new Task("Mow the lawn");
    assertEquals(null, Task.findByID(999));
  }


}
