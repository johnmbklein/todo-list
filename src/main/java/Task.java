import java.util.ArrayList;
import java.time.LocalDateTime;

public class Task {
  private String mName;
  private boolean mCompleted;
  private LocalDateTime mCreatedAt;
  private LocalDateTime mCompletedAt;
  private int mID;
  private static ArrayList<Task> taskArray = new ArrayList<Task>();

  public Task(String name) {
    mName = name;
    mCompleted = false;
    mCreatedAt = LocalDateTime.now();
    // mCompletedAt
    taskArray.add(this);
    mID = taskArray.size();
  }

  public static ArrayList<Task> all() {
    return taskArray;
  }

  public static void clear() {
    taskArray.clear();
  }

  public static Task findByID(int id) {
    try {
      return taskArray.get(id - 1);
    } catch (IndexOutOfBoundsException e) {
      return null;
    }    
  }

  public String getName() {
    return mName;
  }

  public boolean isCompleted() {
    return mCompleted;
  }

  public LocalDateTime getCreatedAt() {
    return mCreatedAt;
  }

  public int getID() {
    return mID;
  }

}
