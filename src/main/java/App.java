import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;


public class App {
  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    get("/", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/index.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/tasks/new", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/new-task.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/tasks", (request, response) -> {
        HashMap model = new HashMap();
        model.put("tasks", Task.all());
        model.put("template", "templates/tasks.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    post("/tasks", (request, response) -> {
        HashMap model = new HashMap();
        String name = request.queryParams("name");
        Task newTask = new Task(name);
        model.put("template", "templates/success.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/tasks/:id", (request, response) -> {
      HashMap model = new HashMap();
      Task newTask = Task.findByID(Integer.parseInt(request.params(":id")));
      model.put("task", newTask);
      model.put("template", "templates/task.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

  }
}
